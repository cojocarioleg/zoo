public class Animal {
    public static void main(String[] args) {
        ObiectAnimal.Message();
        // first animal
        ObiectAnimal Animal_1 = new ObiectAnimal("horse", "white", "2,3m", "123,50kg", 3 );
        // second animal
        ObiectAnimal Animal_2 = new ObiectAnimal ("monkey", "gray", "1,3m", "47,5kg", 5);
        // third animal
        ObiectAnimal Animal_3 = new ObiectAnimal ("Rabit", "gray", "0,3m", "47,5kg", 5);
         //Fourth animal
        ObiectAnimal Animal_4 = new ObiectAnimal ("Penguin", "blak", "0,30m", "4,2kg", 2);
        //fifth animal
        ObiectAnimal Animal_5 = new ObiectAnimal ("Bear", "gray", "1.80m", "70kg", 4);
        //sixth animal
        ObiectAnimal Animal_6 = new ObiectAnimal ("Zebra", "blak/white", "1.80m", "80kg", 6);
        //seventh animal
        ObiectAnimal Animal_7 = new ObiectAnimal ("Wolf", "gray", "0.40m", "30kg", 7);
        //eighth animal
        ObiectAnimal Animal_8 = new ObiectAnimal ("Deer", "yellow", "0.80m", "60kg", 2);
        //ninth animal
        ObiectAnimal Animal_9 = new ObiectAnimal ("Ostrich", "brown", "1.20m", "90kg", 8);
        //tenth animal
        ObiectAnimal Animal_10 = new ObiectAnimal ("Peacock","colored", "0.30m", "20kg", 2);
    }
}
class ObiectAnimal{
    String Name, color,Height, weight;
    int age;
    static void Message(){
        System.out.println("Hello, this is my zoo");
        System.out.println("Name      Color    Height   Weight   Age");
    }
    public ObiectAnimal(String Name, String color, String Height, String weight, int age){
        this.Name = Name;
        this.color = color;
        this.Height = Height;
        this.weight = weight;
        this.age = age;
        System.out.println( this.Name + "    " + this.color + "   " + this.Height + "   " + this.weight + "   " + this.age );

    }
}

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).